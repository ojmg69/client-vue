export const EventService = {
    getEventsData() {
        return [
            {
                "id":'4564561234343',
                "title": "holaaaa",
                "start": "2024-01-15",
                "end": "2024-01-15",
                "color": "red",
                "allDay":"true",
                "className": "hover:cursor-pointer font-bold",
                "extendedProps": {
                  "tipo": "INGRESO",
                  "monto": "40.50",
                  "nota": "hola mundo",
                  "color": "red",
                  "className": "hover:cursor-pointer font-bold"
                  }
            },
            {
                "id":'4564561234',
                "title": "hola",
                "start": "2024-01-16",
                "end": "2024-01-16",
                "color": "green",
                "allDay":"true",
                "className": "hover:cursor-pointer font-bold",
                "extendedProps": {
                  "tipo": "INGRESO",
                  "monto": "55.50",
                  "nota": "hola mundo",
                  "color": "green",
                  "className": "hover:cursor-pointer font-bold"
                  }
            },
            ];
    },

    getProductsMini() {
        return Promise.resolve(this.getEventsData().slice(0, 5));
    },

    getProductsSmall() {
        return Promise.resolve(this.getEventsData().slice(0, 10));
    },

    getProducts() {
        return Promise.resolve(this.getEventsData());
    },

    getProductsWithOrdersSmall() {
        return Promise.resolve(this.getProductsWithOrdersData().slice(0, 10));
    },

    getProductsWithOrders() {
        return Promise.resolve(this.getProductsWithOrdersData());
    }
};

