import { defineStore } from 'pinia';
import { router } from "@/router";
//import { useRoute } from 'vue-router';
import axios from 'axios';

const baseUrl = `${import.meta.env.VITE_API_URL}`;
//const route=useRoute();
export const useAuthStore = defineStore("auth", {
    id: 'auth',
    state: () => ({
        // initialize state from local storage to enable user to stay logged in
        user: JSON.parse(localStorage.getItem('user')),
        srftoken: '',
        returnUrl: null,
        submitting: false,
        errors: ''
    }),
    actions: {

        //login
        async login(email, password) {
            this.errors = '';
            return await axios.post(`${baseUrl}/auth/login`, {
                email,
                password
            },

                {
                    //                    withCredentials:true,
                    headers:
                    {
                        "Accept": "application/json",
                        "Access-Control-Allow-Origin": "*",
                    }
                }).then(RESPONSE => {
                    // update pinia state
                    this.user = RESPONSE.data.user;
                    // store user details and jwt in local storage to keep user logged in between page refreshes
                    localStorage.setItem('user', JSON.stringify(RESPONSE.data.user));
                    localStorage.setItem('srftoken', JSON.stringify(RESPONSE.data.token));
                    // redirect to previous url or default to home page
                    router.push('/perfil');
                }).catch(ERROR => {

                    throw ERROR

                });
        },

        //registro
        async register(username, email, password) {
            this.errors = '';
            return await axios.post(`${baseUrl}/auth/register`, {
                username,
                email,
                password
            },
                {
                    headers:
                    {
                        "Accept": "application/json",
                        "Access-Control-Allow-Origin": "*"

                    }
                }).then(RESPONSE => {
                    // update pinia state
                    this.user = RESPONSE.data.user;

                    // store user details and jwt in local storage to keep user logged in between page refreshes
                    localStorage.setItem('user', JSON.stringify(RESPONSE.data.user));
                    localStorage.setItem('srftoken', JSON.stringify(RESPONSE.data.token));

                    router.push('/perfil');
                }).catch(ERROR => {

                    throw ERROR
                });
        },

        async changePassword(password, id) {
            this.srftoken = JSON.parse(localStorage.getItem('srftoken'));
            axios.post(`${baseUrl}/user/password/${id}`, {
                password
            },
                {
                    headers:
                    {
                        "Accept": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        Authorization: `Bearer ${this.srftoken}`
                    }
                }
            )
                .then(RESPONSE => {

                }).catch(ERROR => {

                    console.log(ERROR);

                }).then(() => {

                });
        },
        //asignando nueva contraseña
        async resetPassword(dato) {
            return await axios.post(`${baseUrl}/auth/reset/password`, {
                email: dato.email,
                token: dato.token,
                password: dato.password,
                password_confirmation: dato.password,
            })
                .then(RESPONSE => {

                }).catch(ERROR => {

                    //console.log(ERROR.response.data.errors);
                    throw ERROR;

                });
        },

        //actualizar datos usuario
        async updateUser(dato, id) {
            this.srftoken = JSON.parse(localStorage.getItem('srftoken'));
            return await axios.post(`${baseUrl}/user/update/${id}`, {
                name: dato.nombre,
                lastname: dato.apellido,
                description: dato.descripcion,
                username: dato.username,
            }, { headers: { Authorization: `Bearer ${this.srftoken}` } })
                .then(RESPONSE => {
                    localStorage.setItem('user', JSON.stringify(RESPONSE.data.user));
                    this.user = RESPONSE.data.user;
                }).catch(ERROR => {
                    throw ERROR;

                })
        },

        async sendMail(email) {
            return await axios.post(`${baseUrl}/auth/reset-password`, {
                email
            })
                .then(RESPONSE => {

                }).catch(ERROR => {

                    throw ERROR;

                })
        },

        //enviar mensaje contacto
        async sendMailContact(name, email, message) {
            return await axios.post(`${baseUrl}/sendmail`, {
                name,
                email,
                message,
            })
                .then(RESPONSE => {

                }).catch(ERROR => {

                    throw ERROR;

                })
        },


        async logout() {
            this.srftoken = JSON.parse(localStorage.getItem('srftoken'));
            this.submitting=true;
            await axios.post(`${baseUrl}/logout?token=${this.srftoken}`)
                .then(RESPONSE => {
                    this.user = null;
                    this.atoken = null;
                    localStorage.removeItem('user');
                    localStorage.removeItem('srftoken');
                    router.push('/');

                }).catch(ERROR => {

                    console.log(ERROR);

                }).finally(() => {
                    this.submitting=false;
                });

        },
        setSubmitting(vl) {
            this.submitting = vl;
        }
    },

    getters: {
        getErrors(state) {
            return state.errors;
        },
        isSubmitting(state) {
            return state.submitting;
        },


    },


})
