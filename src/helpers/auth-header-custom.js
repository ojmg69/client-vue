export default function authHeaderC() {
    let user = JSON.parse(localStorage.getItem('user'));
    let srftoken =JSON.parse(localStorage.getItem('srftoken'));
  
    if (srftoken) {
      // for Node.js Express back-end
      //return { 'x-access-token': user.accessToken };
      return { Authorization:  `Bearer ${srftoken}` };
    } else {
      return {};
    }
  }