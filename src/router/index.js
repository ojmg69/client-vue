import { createRouter, createWebHistory } from 'vue-router';

import { useAuthStore } from '@/stores';

import { Inicio,NosotrosPage,ContactoPage,LoginPage,PasswordPage,ResetPage,RegisterPage,Perfil,
 Calendario,Balance} from '@/pages';


export const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    linkActiveClass: 'active',
    
    routes: [
        
        { path: '/', component: Inicio },
        { path: '/nosotros', component: NosotrosPage },
        { path: '/contacto', component: ContactoPage },
        { path: '/login', component: LoginPage },
        {path:'/newpassword/:email/:token',  component: PasswordPage  },
        { path: '/reset', component: ResetPage },
        { path: '/register', component: RegisterPage },

        { path: '/perfil', component: Perfil, meta: { requiresAuth: true }, },
        { path: '/calendario', component: Calendario, meta: { requiresAuth: true }, },
        { path: '/balance', component: Balance, meta: { requiresAuth: true }, },
        //{ ...usersRoutes },
        // catch all redirect to home page
        //{ path: '/:pathMatch(.*)*', redirect: '/' }
    ]
});

router.beforeEach(async (to) => {

    const authStore = useAuthStore();
    if (to.meta.requiresAuth && !authStore.user) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        return {
          path: '/login',
          // save the location we were at to come back later
          //query: { redirect: to.fullPath },
        }
        }
    
});
